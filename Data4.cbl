       ID DIVISION. 
       PROGRAM-ID. Data4.
       AUTHOR. KUNTAPONG.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STUDENT-REC-DATA PIC X(44) VALUE "1205621WIlliam  Fitzpatrick
      -    " 19751021LM051385".
       01  LONG-STRING PIC X(200) VALUE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      -    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      -    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".
       01  STUDENT-REC.
           05 STUDENT-ID     PIC 9(7).
           05 STUDENT-NAME.
              10 FORNAME     PIC X(9).
              10 SURNAME.
                 15 F-SURNAME PIC X(1).
                 15 FILLER      PIC X(11).
           05 DATE-OF-BIETH.
              08 YOB         PIC 9(4).
              08 MOB-DOB.
                 10 MOB         PIC 99.
                 10 DOB         PIC 99.
           05 COURSE-ID      PIC X(5).
           05 GPA            PIC 9V99.
       PROCEDURE DIVISION .
           DISPLAY STUDENT-REC-DATA 
           MOVE STUDENT-REC-DATA TO STUDENT-REC.
           DISPLAY STUDENT-REC 
           DISPLAY STUDENT-ID 
           DISPLAY STUDENT-NAME 
           DISPLAY FORNAME 
           DISPLAY SURNAME 
           DISPLAY F-SURNAME "." FORNAME
           DISPLAY DATE-OF-BIETH 
           DISPLAY DOB"/"MOB"/"YOB
           DISPLAY COURSE-ID 
           DISPLAY GPA 
           DISPLAY MOB-DOB 
           .
