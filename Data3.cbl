       ID DIVISION. 
       PROGRAM-ID. Data3.
       AUTHOR. KUNTAPONG.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SURNAME           PIC   X(8)  VALUE "COUGHLAN".
       01  SALE-PRICE        PIC   9(4)V99.
       01  NUM-OF-EMPLOYESS  PIC   999V99.
       01  SALARY            PIC   9999V99.
       01  COUNTY-NAME       PIC   X(99).
       PROCEDURE DIVISION .
       Begin.
           DISPLAY "1 " SURNAME
           MOVE "SMITH" TO SURNAME
           DISPLAY "2 " SURNAME 
           MOVE "FITZWILLIAM" TO SURNAME
           DISPLAY "3 " SURNAME 
           .
           DISPLAY "1 " SALE-PRICE 
           MOVE ZEROS TO SALE-PRICE 
           DISPLAY "2 " SALE-PRICE 
           MOVE 25.5 TO SALE-PRICE
           DISPLAY "3 " SALE-PRICE 
           MOVE 7.553 TO SALE-PRICE
           DISPLAY "4 " SALE-PRICE 
           MOVE 93425.158 TO SALE-PRICE
           DISPLAY "5 " SALE-PRICE 
           MOVE 128 TO SALE-PRICE
           DISPLAY "6 " SALE-PRICE 
           .
           DISPLAY NUM-OF-EMPLOYESS 
           MOVE 12.4 TO NUM-OF-EMPLOYESS
           DISPLAY NUM-OF-EMPLOYESS 
           MOVE 6745 TO NUM-OF-EMPLOYESS
           DISPLAY NUM-OF-EMPLOYESS 
           MOVE NUM-OF-EMPLOYESS TO SALARY
           DISPLAY SALARY 
           .
           MOVE "GALWAY" TO COUNTY-NAME
           DISPLAY COUNTY-NAME 
           MOVE ALL "KOB_" TO COUNTY-NAME
           DISPLAY COUNTY-NAME 
           .
